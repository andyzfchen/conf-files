syntax on

set tabstop=2
set shiftwidth=2
set expandtab
set number
set relativenumber
set ruler
set showtabline=2

set hlsearch
set incsearch

set cursorline
set cursorcolumn
highlight CursorLine cterm=none ctermbg=236
highlight CursorColumn cterm=none ctermbg=236
