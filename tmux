set-option -g allow-rename off

run-shell ~/Documents/github/tmux-resurrect/resurrect.tmux

# http://www.deanbodenham.com/learn/tmux-conf-file.html
#-------------------------------------------------------#
#Pane colours
#-------------------------------------------------------#
# set inactive/active window styles
set -g window-style fg=colour247,bg=colour236
set -g window-active-style fg=colour250,bg=black
 
#pane border
#set -g pane-border-bg colour235
#set -g pane-border-fg colour238
set -g pane-border-style bg=colour235,fg=colour238
#set -g pane-active-border-bg colour236
#set -g pane-active-border-fg colour51
set -g pane-active-border-style bg=colour236,fg=colour51
#-------------------------------------------------------#

#-------------------------------------------------------#
#PANE NAVIGATION/MANAGEMENT
#-------------------------------------------------------#
# Use Alt-arrow keys WITHOUT PREFIX KEY to switch panes
bind h select-pane -L
bind l select-pane -R
bind k select-pane -U
bind j select-pane -D
#-------------------------------------------------------#

#-------------------------------------------------------#
##STATUS LINE/MESSAGES AT BOTTOM
#-------------------------------------------------------#

# Set background of status line to black
#-------------------------------------------------------#
#set -g status-bg blue
set -g status-bg black


#Colours for messages
#-------------------------------------------------------#
# enable activity alerts
setw -g monitor-activity on
set -g visual-activity on

#Show reloaded message in bright white 
#set -g message-fg white
#set -g message-bg default
#set -g message-attr bright
set -g message-style fg=white,bg=default,bright
#-------------------------------------------------------#


# Status line left side
#-------------------------------------------------------#
# Session: 0 1 1 settings
set -g status-left-length 40 
set -g status-left "#[fg=colour245] #S #[fg=colour84]#I #[fg=colour75]#P"
#-------------------------------------------------------#

#Window/pane type in centre
#-------------------------------------------------------#
#move bash* to centre
set -g status-justify centre

# set the color of the window list
# The name of the pane
#setw -g window-status-fg colour75
#setw -g window-status-bg black
#setw -g window-status-attr dim
setw -g window-status-style fg=colour75,bg=black
setw -g window-status-current-style fg=white,bg=black
#-------------------------------------------------------#
