# turn off bash depreciation warning
export BASH_SILENCE_DEPRECATION_WARNING=1

# User specified commands
cs() { cd "$1" && ls; }
sshumt3() { ssh "$1"@umt3int0"$2".aglt2.org; }
sshumt3y() { ssh -Y "$1"@umt3int0"$2".aglt2.org; }
sshlxplus() { ssh "$1"@lxplus795.cern.ch; }
sshlxplusy() { ssh -Y "$1"@lxplus795.cern.ch; }
sshfelix() { ssh "$1"@um-felix"$2".cern.ch; }
sshfelixy() { ssh -Y "$1"@um-felix"$2".cern.ch; }
sshwebserver() { ssh -l root 45.76.76.117; }
condafp() { conda activate funProjects; }
condaeh() { conda activate exoticHiggs; }
condab() { conda activate base; }
