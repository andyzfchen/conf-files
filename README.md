# Customized Configuration Files #

This repository contains snippets of configuration files used by the author's most frequently used applications.

## Tmux ##

To download the tmux package:  
[https://github.com/tmux/tmux/wiki](https://github.com/tmux/tmux/wiki)

To download the tmux-resurrect plugin:  
[https://github.com/tmux-plugins/tmux-resurrect](https://github.com/tmux-plugins/tmux-resurrect)

## Ergodone Mapping ##

To create a new or modify an old key mapping json file:  
[https://config.qmk.fm/#/ergodone/LAYOUT\_ergodox](https://config.qmk.fm/#/ergodone/LAYOUT_ergodox)

To find a keycode:  
[https://docs.qmk.fm/#/keycodes](https://docs.qmk.fm/#/keycodes)

To flash the json into the keyboard memory:  
[https://tkg.io/#](https://tkg.io/#)
